﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RoleBasedAppAccess.Models
{
        public class Item
        {
            [Key]
            public int Id { get; set; }
            [Required]
            [Display(Name = "Name")]
            [StringLength(20, MinimumLength = 3)]
            public string Name { get; set; }

            [Required]
            [Display(Name = "Price")]
            public int Price { get; set; }

            [Required]
            [Display(Name = "Description")]
            [StringLength(20, MinimumLength = 3)]
            public string Description { get; set; }

            public string Category { get; set; }
            public int Offer { get; set; }
            public string ImagePath { get; set; }
        }

        public class Cart
        {
            [Key]
            public int Id { get; set; }
            public string UserName { get; set; }
            public int ItemId { get; set; }
        }
   
}