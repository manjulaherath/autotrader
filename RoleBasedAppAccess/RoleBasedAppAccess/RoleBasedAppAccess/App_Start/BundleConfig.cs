﻿using System.Web;
using System.Web.Optimization;

namespace RoleBasedAppAccess
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.UseCdn = true;

            var cdnPath = "http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100";
            bundles.Add(new StyleBundle("~/Linkfonts", cdnPath));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/jquery-1.11.0.min.js",
                      "~/Scripts/jquery.cookie.js",
                      "~/Scripts/waypoints.min.js",
                      "~/Scripts/bootstrap-hover-dropdown.js",
                      "~/Scripts/owl.carousel.min.js",
                      "~/Scripts/front.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/style.default.css",
                      "~/Content/animate.min.css",
                      "~/Content/font-awesome.css",
                      "~/Content/owl.carousel.css",
                      "~/Content/owl.theme.css",
                      "~/Content/PagedList.css",
                      "~/Content/custom.css"
                      ));

            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = true;
        }
    }
}
