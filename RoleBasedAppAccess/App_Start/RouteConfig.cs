﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace RoleBasedAppAccess
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "AdminViewGallery",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Admin", action = "Index"}
            );

            routes.MapRoute(
                name: "AdminShoppingCart",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "ShoppingCart", action = "Index" }
            );

            routes.MapRoute(
                name: "AccountSection",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Account", action = "Index" }
            );

            routes.MapRoute(
                name: "ShoppingCartAdd",
                url: "ShoppingCart/AddCart/",
                defaults: new { controller = "ShoppingCart", action = "AddCart" }
            );

            routes.MapRoute(
                name: "ShoppingBasket",
                url: "ShoppingCart/GetCartItems/",
                defaults: new { controller = "ShoppingCart", action = "GetCartItems" }
            );

            routes.MapRoute(
                name: "ManageSection",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Manage", action = "Index" }
            );
            
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
