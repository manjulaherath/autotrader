﻿using System.Web;
using System.Web.Optimization;

namespace RoleBasedAppAccess
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.UseCdn = true;
                         
             bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

             bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/jquery-1.11.1.min.js",
                      "~/Scripts/jquery.cookie.js",
                      "~/Scripts/waypoints.min.js",
                      "~/Scripts/bootstrap-hover-dropdown.js",
                      "~/Scripts/owl.carousel.min.js",
                      "~/Scripts/front.js",
                      "~/Scripts/bootstrap-image-gallery.min.js" ,
                      "~/Scripts/jquery-sidebar.js" 
                     ));

            bundles.Add(new ScriptBundle("~/bundles/gallery").Include(
                      "~/Scripts/jquery.blueimp-gallery.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/style.default.css",
                      "~/Content/animate.min.css",
                      "~/Content/font-awesome.css",
                      "~/Content/owl.carousel.css",
                      "~/Content/owl.theme.css",
                      "~/Content/PagedList.css",
                      "~/Content/custom.css",
                      "~/Content/searchcss.css",
                      "~/Content/bootstrap-image-gallery.min.css",
                      "~/Content/blueimp-gallery.min.css",
                      "~/Content/ladda-themeless.min.css",
                      "~/Content/flexslider.css"
                      ));


            bundles.Add(new ScriptBundle("~/bundles/jqueryFileUpload").Include(
                       "~/Scripts/jquery.min-Fileupload.js",
                       "~/Scripts/jquery-ui.min-FileUpload.js"
                       ));

            bundles.Add(new ScriptBundle("~/bundles/header").Include(
                       "~/Scripts/megamenu.js",
                       "~/Scripts/menu_jquery.js"));

            bundles.Add(new StyleBundle("~/bundles/headerCss").Include(
                       "~/Content/megamenu.css"
                       ));



                                            
            // Set EnableOptimizations to false for debugging. For more information,
            // visit http://go.microsoft.com/fwlink/?LinkId=301862
            BundleTable.EnableOptimizations = true;
        }
    }
}
