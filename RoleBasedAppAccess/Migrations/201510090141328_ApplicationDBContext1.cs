namespace RoleBasedAppAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ApplicationDBContext1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Items", "Name", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Items", "Description", c => c.String(nullable: false, maxLength: 20));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Items", "Description", c => c.String());
            AlterColumn("dbo.Items", "Name", c => c.String());
        }
    }
}
