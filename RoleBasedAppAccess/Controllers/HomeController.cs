﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using RoleBasedAppAccess.Models;

namespace RoleBasedAppAccess.Controllers
{
    public class HomeController : Controller
    {
        DataContext appContext = new DataContext();
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                string getName = User.Identity.Name;
                int count = appContext.Cart.Where(s => s.UserName == getName).Count();
                Session["cartCount"] = count;

            }
            return View(getAllItems());
           
        }

        public IEnumerable<Item> getAllItems()
        {
            IEnumerable<Item> AllItems = from c in appContext.Item
                                         select c;

            return AllItems;
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult SearchYourDreamVehicle()
        {

            //string html = new WebClient().DownloadString("http://jpnauction.com/aj_neo?classic");
            return View();
        }


    }
}