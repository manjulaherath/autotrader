﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using RoleBasedAppAccess.Models;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net.Mime;
using System.Net;
using RoleBasedAppAccess.CustomFilters;
using System.Web.Security;
using PagedList;
using System.IO;
using MvcFileUploader.Models;
using MvcFileUploader;
using System.Data.Entity;

namespace RoleBasedAppAccess.Controllers
{
    public class AdminController : Controller
    {

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;


        ApplicationDbContext context;
        DataContext appContext;

        public AdminController()
        {
            context = new ApplicationDbContext();
            appContext = new DataContext();
        }

        public AdminController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        [AuthLog(Roles = "Administrator")]
        public ActionResult Index()
        {
            return View();
        }


        public IEnumerable<Item> getAllItems()
        {
            IEnumerable<Item> AllItems = from c in appContext.Item
                                         select c;

            return AllItems;
        }
        
        [AuthLog(Roles = "Administrator")]
        public ActionResult ItemManagement(
             string sortOrder, string currentFilter, string searchString, int? page)
        {

            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var Items = from s in appContext.Item
                        select s;
            
            if (!String.IsNullOrEmpty(searchString))
            {
                Items = Items.Where(s => s.Name.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "name_desc":
                    Items = Items.OrderByDescending(s => s.Id);
                    break;
                default:  // Name ascending 
                    Items = Items.OrderBy(s => s.Id);
                    break;
            }

            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(Items.ToPagedList(pageNumber, pageSize));

        }

        [AuthLog(Roles = "Administrator")]
        public ActionResult AddItem(string id)
        {
           return View();
        }

        [AuthLog(Roles = "Administrator")]
        [HttpPost]
        public ActionResult AddItem(Item ItemDetails)
        {
            string message = "That Item name has already been used";

            var existingItemCount = appContext.Item.Count(a => a.Name.Equals(ItemDetails.Name));

            try
            {
                if (ModelState.IsValid)
                {
                    if (existingItemCount > 0)
                    {
                        ViewBag.Message = message;
                        return View(ItemDetails);
                    }
                    else
                    {
                        appContext.Item.Add(ItemDetails);
                        appContext.SaveChanges();

                        //return RedirectToAction("Edit", "Item", new { id = ItemDetails.Id });
                    }
                }
            }
            catch (Exception ex)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.)
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
            }

            return View(ItemDetails);
        }

        public ActionResult AddItemImage(string id)
        {
            Session["ItemId"] = id;
            return View();
        }

        public ActionResult ItemUploadFile(int? entityId) // optionally receive values specified with Html helper
        {
            string subPath = string.Empty;
            string FileName = string.Empty;
            // here we can send in some extra info to be included with the delete url 
            var statuses = new List<ViewDataUploadFileResult>();
            for (var i = 0; i < Request.Files.Count; i++)
            {
                var st = FileSaver.StoreFile(x =>
                {
                    x.File = Request.Files[i];
                    //note how we are adding an additional value to be posted with delete request
                    //and giving it the same value posted with upload
                    FileName=x.File.FileName;

                    x.DeleteUrl = Url.Action("DeleteFile", new { entityId = entityId });

                    subPath = "/Content/uploads/Items/" + Session["ItemId"].ToString(); // your code goes here

                    bool exists = System.IO.Directory.Exists(Server.MapPath(subPath));

                    if (!exists)
                        System.IO.Directory.CreateDirectory(Server.MapPath(subPath));

                    x.StorageDirectory = Server.MapPath(subPath);
                    x.UrlPrefix = "/Content/uploads/Items/" + Session["ItemId"].ToString();// this is used to generate the relative url of the file

                    //overriding defaults
                    x.FileName = Request.Files[i].FileName;// default is filename suffixed with filetimestamp
                    x.ThrowExceptions = true;//default is false, if false exception message is set in error property
                });

                statuses.Add(st);
            }

            //statuses contains all the uploaded files details (if error occurs then check error property is not null or empty)
            //todo: add additional code to generate thumbnail for videos, associate files with entities etc

            //adding thumbnail url for jquery file upload javascript plugin
            statuses.ForEach(x => x.thumbnailUrl = x.url + "?width=63&height=80"); // uses ImageResizer httpmodule to resize images from this url

            //setting custom download url instead of direct url to file which is default
            statuses.ForEach(x => x.url = Url.Action("DownloadFile", new { fileUrl = x.url, mimetype = x.type }));


            //server side error generation, generate some random error if entity id is 13
            if (entityId == 13)
            {
                var rnd = new Random();
                statuses.ForEach(x =>
                {
                    //setting the error property removes the deleteUrl, thumbnailUrl and url property values
                    x.error = rnd.Next(0, 2) > 0 ? "We do not have any entity with unlucky Id : '13'" : String.Format("Your file size is {0} bytes which is un-acceptable", x.size);
                    //delete file by using FullPath property
                    if (System.IO.File.Exists(x.FullPath)) System.IO.File.Delete(x.FullPath);
                });
            }
            else
            {
                //update image path in Item Table
                int ItemId = Convert.ToInt32(Session["ItemId"]);
                var Item = appContext.Item.First(r => r.Id == ItemId);
                Item.ImagePath = subPath + "/" + FileName;
                appContext.Entry(Item).State = EntityState.Modified;
                appContext.SaveChanges();
            }

            var viewresult = Json(new { files = statuses });
            //for IE8 which does not accept application/json
            if (Request.Headers["Accept"] != null && !Request.Headers["Accept"].Contains("application/json"))
                viewresult.ContentType = "text/plain";

            

            return viewresult;
        }


        [AuthLog(Roles = "Administrator")]
        public ActionResult ManageUserInfo(string id)
        {

            Session["UserId"] = id;

            return View();
        }

        [AuthLog(Roles = "Administrator")]
        public ActionResult ViewGallery(string id="")
        {
            Session["UserId"] = id;

            var model = new CustomerImageViewModel()
            {
                Images = Directory.EnumerateFiles(Server.MapPath("~/Content/Uploads/"+ Session["UserId"].ToString()))
                                  .Select(fn => "~/Content/Uploads/"+ Session["UserId"].ToString() + "/" + Path.GetFileName(fn))
            };
            return View(model);
        }

        public ActionResult UploadFile(int? entityId) // optionally receive values specified with Html helper
        {
            // here we can send in some extra info to be included with the delete url 
            var statuses = new List<ViewDataUploadFileResult>();
            for (var i = 0; i < Request.Files.Count; i++)
            {
                var st = FileSaver.StoreFile(x =>
                {
                    x.File = Request.Files[i];
                    //note how we are adding an additional value to be posted with delete request
                    //and giving it the same value posted with upload
                    x.DeleteUrl = Url.Action("DeleteFile", new { entityId = entityId });

                    string subPath = "~/Content/uploads/"+ Session["UserId"].ToString(); // your code goes here

                    bool exists = System.IO.Directory.Exists(Server.MapPath(subPath));

                    if (!exists)
                        System.IO.Directory.CreateDirectory(Server.MapPath(subPath));

                    x.StorageDirectory = Server.MapPath(subPath);
                    x.UrlPrefix = "/Content/uploads/" + Session["UserId"].ToString();// this is used to generate the relative url of the file
                    
                    //overriding defaults
                    x.FileName = Request.Files[i].FileName;// default is filename suffixed with filetimestamp
                    x.ThrowExceptions = true;//default is false, if false exception message is set in error property
                });

                statuses.Add(st);
            }

            //statuses contains all the uploaded files details (if error occurs then check error property is not null or empty)
            //todo: add additional code to generate thumbnail for videos, associate files with entities etc

            //adding thumbnail url for jquery file upload javascript plugin
            statuses.ForEach(x => x.thumbnailUrl = x.url + "?width=63&height=80"); // uses ImageResizer httpmodule to resize images from this url

            //setting custom download url instead of direct url to file which is default
            statuses.ForEach(x => x.url = Url.Action("DownloadFile", new { fileUrl = x.url, mimetype = x.type }));


            //server side error generation, generate some random error if entity id is 13
            if (entityId == 13)
            {
                var rnd = new Random();
                statuses.ForEach(x =>
                {
                    //setting the error property removes the deleteUrl, thumbnailUrl and url property values
                    x.error = rnd.Next(0, 2) > 0 ? "We do not have any entity with unlucky Id : '13'" : String.Format("Your file size is {0} bytes which is un-acceptable", x.size);
                    //delete file by using FullPath property
                    if (System.IO.File.Exists(x.FullPath)) System.IO.File.Delete(x.FullPath);
                });
            }

            var viewresult = Json(new { files = statuses });
            //for IE8 which does not accept application/json
            if (Request.Headers["Accept"] != null && !Request.Headers["Accept"].Contains("application/json"))
                viewresult.ContentType = "text/plain";

            return viewresult;
        }



        //here i am receving the extra info injected
        [HttpPost] // should accept only post
        public ActionResult DeleteFile(int? entityId, string fileUrl)
        {
            var filePath = Server.MapPath("~" + fileUrl);

            if (System.IO.File.Exists(filePath))
                System.IO.File.Delete(filePath);

            var viewresult = Json(new { error = String.Empty });
            //for IE8 which does not accept application/json
            if (Request.Headers["Accept"] != null && !Request.Headers["Accept"].Contains("application/json"))
                viewresult.ContentType = "text/plain";

            return viewresult; // trigger success
        }

        [AuthLog(Roles = "Administrator")]
        public ActionResult DownloadFile(string fileUrl, string mimetype)
        {
            var filePath = Server.MapPath("~" + fileUrl);

            if (System.IO.File.Exists(filePath))
                return File(filePath, mimetype);
            else
            {
                return new HttpNotFoundResult("File not found");
            }
        }

        //[HttpPost] // should accept only post
        [AuthLog(Roles = "Administrator")]
        public ActionResult DeleteUploadedImage(string fileUrl)
        {
            var filePath = Server.MapPath(fileUrl);

            if (System.IO.File.Exists(filePath))
                System.IO.File.Delete(filePath);

            var model = new CustomerImageViewModel()
            {
                Images = Directory.EnumerateFiles(Server.MapPath("~/Content/Uploads/" + Session["UserId"].ToString()))
                                  .Select(fn => "~/Content/Uploads/" + Session["UserId"].ToString() + "/" + Path.GetFileName(fn))
            };

            return PartialView("ViewGallery", model);

            
        }

        [AuthLog(Roles = "Administrator")]
        public ActionResult ViewCustomers(
             string sortOrder, string currentFilter, string searchString, int? page)
        {

            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var role = (from r in context.Roles where r.Name.Contains("User") select r).FirstOrDefault();
            var users = context.Users.Where(x => x.Roles.Select(y => y.RoleId).Contains(role.Id)).ToList();

            if (!String.IsNullOrEmpty(searchString))
            {
                users = users.ToList().Where(x => x.Email.Contains(searchString)).ToList();
            }

            int pageSize = 1;
            int pageNumber = (page ?? 1);
            return View(users.ToPagedList(pageNumber, pageSize));

        }

    }

   

}